

<div align="center">

<img src="https://gitlab.com/viknes10/idp-logbook-1/-/raw/main/R__1_.png" width=200 align=middle>

</div>




<div align='center'>

**DEPARTMENT OF AEROSPACE ENGINEERING**

**EAS4947-1:INTEGRATED DESIGN PROJECT**

</div>

<div align="center">

**Weekly Log Repository**

</div>

<br />
<br />
<br />


## Front cover 

| Heading | Information |
| ------ | ----------- |
| Name | Roshan George Daniel |
| Matric Number   | 197454 |
| Year of Study  | Final Year |
| Subsystem Group | IDP - Software and Control Group |
| Team   | 1 |
| Lecturer / Supervisor   | DR. AHMAD SALAHUDDIN BIN MOHD HARITHUDDIN |
<br />
<br />
<br />

## Contents of the Log

| Log Book Content | 
| ------ | 
| 1. Agenda   |
| 2. Goals   |
| 3. Problems   |
| 4. Decisions taken to solve problems   |
| 5. Method to solve problems   |
| 6. Justification   |
| 7. Impact of the decision taken into   |
| 8. Next progress or step   |
<br />
<br />
<br />


### The log contains my progress for every week during the Integrated Design Project



| Shortcut to Weekly Logs | 
| :-----------: | 
| [Week 3](https://gitlab.com/roshangd/weekly-log/-/blob/main/README.md#user-content-log-week-3-5112021) |
| [Week 4](https://gitlab.com/roshangd/weekly-log/-/blob/main/README.md#user-content-log-week-4-12112021) | 
| [Week 5](https://gitlab.com/roshangd/weekly-log/-/blob/main/README.md#user-content-log-week-5-19112021) | 
| [Week 6](https://gitlab.com/roshangd/weekly-log/-/blob/main/README.md#user-content-log-week-6-26112021) | 
| [Week 7](https://gitlab.com/roshangd/weekly-log/-/blob/main/README.md#user-content-log-week-7-2122021) | 

<br />
<br />
<br />


## Log Week 3 (5/11/2021)



### Action

- Took some time to tinker with MATLAB and Simulink. 
- Read a few research papers regarding to the Control Systems of High Altitude Airship.

### Agenda/Goals

- The agenda of the week to learn as much as possible about the basic control systems in a rotorcraft 


### Methods

- We had researched and shared our research papers in the Whatsapp Group so that our members could have a look at it.
- The research papers were obtain online and also given by our lecturers.

### Justification

The title of the research papers are as the following:  

- Development of attitude control system for hybrid airship vehicle  
- Flight PID Controller Design for a UAV Quadrotor  
- Flying over the reality gap: From simulated to real indoor airships  
- FINLESS AIRSHIP DYNAMICS MODELLING USING SIMULINK   
- Development and validation of a dynamics model for an unmanned finless airship

### Impact

- Since it was the first few weeks, we didnt have much of an impact throughout the whole project

### Problems

- We didn't face any issues this week

### Decisions

- We proceeded with our research throughout a few research paper

### Next Step

- The next step is to derive the block diagrams from the references to understand the control system visually






## Log Week 4 (12/11/2021)




### Actions

- Continued to read research papers on the Control Systems. 
- Proceeded to research block diagram of the Control Subsystems.
- Some members of the team had an initial meet up with Mr.Azizi at the team

### Agenda/Goals

- Our goals this week to draw out the block diagrams for the HAU Control Systems



### Methods

- We refered the block diagrams based on the research papers
- Appointment at the lab

### Justification

- N/A

### Impact

- We are able to provide a visualisation on the software and control systems


### Problems

- We werent able to meet up to make any progress within the team

### Decisions

- We proceeded to set an appointment with Mr.Azizi


### Next Step

- Subsequently, we would like to meet up with Mr.Azizi to figure our next steps within the team
- 





## Log Week 5 (19/11/2021)




### Actions

- We were trying to familiarise ourselves with autopilot softwares such as ArduPilot and PX4 Softwares
- We watched multiple videos on that explain the calibration process on YouTube
- The Team had a meeting with Mr.Azizi where we learnt the calibration process face to face. 
- We did an initial calibration using a control board from a drone.


### Agenda/Goals

- To familiarise ourselves with the softwares 
- To find out how the calibration process works


### Methods

- We met up at the lab to learn how to calibrate using a control board and the Ardupilot Software

### Justification

- N/A

### Impact

- This is our initial point where we have gotten some hands on experience within the software team


### Problems

- During the meet up, not everyone was able to come to the lab

### Decisions

- We decided to have the next meetings where everyone could make it together have a discussion on the tasks 

### Next Step

- Go deeper within the software to find out the functioning methods of the software





## Log Week 6 (26/11/2021)




### Actions

- Mr Azizi had provided us with a task to make a summary
- We divided ourselves into groups and summarize the documentation for the PX4 Software




### Agenda/Goals

- Our goals were to make a documentation so that everyone could understand the software and control system of the HAU



### Methods

- We read the PX4 documentation online and made the documentation on Gitlab


### Justification

The different Subsystems consists of :
-  Basic Concepts
- Flight Controller
- Vehicle/Frames
- Sensors
- Remote Control Systems
- Flight Modes
- Payloads and Cameras
- Flight Reporting


### Impact

- We are able to provide some references to other subsystems on the different sensors used on the HAU


### Problems

- We didnt find much of a problem this week


### Decisions

- N/A



### Next Step

- Our next step is read and understand the codes that are used to that are used in the HAU




## Log Week 7 (2/12/2021)




### Actions

- For this week, Mr.Azizi had asked us to use the Ardupilot as a firmware.
- He had passed on a link on Gitlab to refer to the Ardupilot project.
- We studied and understood the CPP files on the repository


### Agenda/Goals

- The goal was to go through the repository and understand the codes used for autonomous flights



### Methods

- The research was done Gitlab


### Justification

- N/A

### Impact

- There is some progress from knowing the technicalities behind conducting autonomous flights


### Problems

- I feel personally that the team needs a push in contributing to the team


### Decisions

- No Decision taken on this issue as of yet


### Next Step

- To apply the codes on the fully assembled airship

